FROM openjdk:8
COPY "./target/Simple-Interest-0.0.1.jar" "app.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]