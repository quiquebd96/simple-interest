package com.aplazo.mx.interest.model;

import java.util.Date;

import lombok.Data;

@Data
public class Response {
	
	private Integer payment_number;
	private Double amount;
	private Date payment_date;
	
}
