package com.aplazo.mx.interest.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Schema input data")
public class Request {

    @NotNull(message="Field [amount] must not be null")
    @Min(value = 1,message = "The amount should be more than $1.00, the max should be lesser than $999,999.00.")
    @Max(value = 999999, message = "The amount should be more than $1.00, the max should be lesser than $999,999.00.")
    @Schema(required = true, description = "Amount of money" , example="10500")
    private Double amount;

    @NotNull(message="Field [terms] must not be null")
    @Min(value = 4 ,message = "The max terms (weeks) were the payment can be paid is 52, the minimum should be 4.")
    @Max(value = 52, message = "The max terms (weeks) were the payment can be paid is 52, the minimum should be 4.")
    @Schema(required = true, description = "Number of weeks", example="6")
    private Integer terms;

    @NotNull(message="Field [rate] must not be null")
    @Min(value = 1,message = "The rate should bigger than 1%, lesser than 100%.")
    @Max(value = 100, message = "The rate should bigger than 1%, lesser than 100%.")
    @Schema(required = true, description = "Interest for amount", example="2.3")
    private Double rate;

}