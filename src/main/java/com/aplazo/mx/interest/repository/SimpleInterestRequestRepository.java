package com.aplazo.mx.interest.repository;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import com.aplazo.mx.interest.entity.InterestRequest;

@Repository
public interface SimpleInterestRequestRepository extends JpaRepositoryImplementation<InterestRequest,Long>{

}
