package com.aplazo.mx.interest.repository;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import com.aplazo.mx.interest.entity.InterestResponse;

@Repository
public interface SimpleInterestResponseRepository extends JpaRepositoryImplementation<InterestResponse,Long>{

}
