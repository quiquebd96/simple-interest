package com.aplazo.mx.interest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "RESPONSE")
public class InterestResponse {
	
	@Id
    @Column(name = "ID_RESPONSE")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idResponse;
	
	@Column(name = "ID_REQUEST")
	private Long idRequest;
	
	@Column(name = "RESPONSE_JSON")
	private String response_json;
	
	@JoinColumn(name = "ID_REQUEST", insertable = false, updatable = false)
    @OneToOne
    private InterestRequest request;

}
