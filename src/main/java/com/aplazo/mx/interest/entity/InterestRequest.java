package com.aplazo.mx.interest.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "REQUEST")
public class InterestRequest {
	
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "REQUEST_JSON")
	private String request_json;
	
	@OneToOne(mappedBy = "request", cascade = CascadeType.ALL)
    private InterestResponse response;

}
