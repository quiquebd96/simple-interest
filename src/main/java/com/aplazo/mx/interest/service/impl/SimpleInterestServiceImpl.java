package com.aplazo.mx.interest.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aplazo.mx.interest.entity.InterestRequest;
import com.aplazo.mx.interest.model.Request;
import com.aplazo.mx.interest.model.Response;
import com.aplazo.mx.interest.service.InterestRequestService;
import com.aplazo.mx.interest.service.InterestResponseService;
import com.aplazo.mx.interest.service.SimpleInterestService;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class SimpleInterestServiceImpl implements SimpleInterestService{
	
	private static final Logger log = LoggerFactory.getLogger(SimpleInterestServiceImpl.class);
		
	@Autowired
	InterestRequestService interestRequestService;
	
	@Autowired
	InterestResponseService interestResponseService;

	@Override
	public List<Response> getSimpleInterestList(Request request) throws JsonProcessingException {
		
		List<Response> response = new ArrayList<>();
		Double interest = request.getAmount() * (request.getRate() / 100);
		Calendar calendar = Calendar.getInstance();
		
		log.info("Simple Interes ->"+ request.getAmount() * (request.getRate() / 100) * request.getTerms() );
		
		for(int i=0 ; i <=  request.getTerms(); i++) {
			
			Response payResponse = new Response();
			payResponse.setPayment_number(i);
			payResponse.setAmount(  i == 0 ? request.getAmount() : response.get(i-1).getAmount() + interest );
			calendar.add(Calendar.WEEK_OF_YEAR, 1);
			payResponse.setPayment_date( calendar.getTime() );
			response.add(payResponse);	
		}
		
		InterestRequest interest_bd = interestRequestService.saveRequestInteres(request);
		interestResponseService.saveResponseInterest(response, interest_bd.getId());
		
		return response;
	}
	
}
