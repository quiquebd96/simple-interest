package com.aplazo.mx.interest.service;

import com.aplazo.mx.interest.entity.InterestRequest;
import com.aplazo.mx.interest.model.Request;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface InterestRequestService {
	
	InterestRequest saveRequestInteres(Request request) throws JsonProcessingException;
	

}
