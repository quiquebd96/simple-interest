package com.aplazo.mx.interest.service;

import java.util.List;

import com.aplazo.mx.interest.model.Request;
import com.aplazo.mx.interest.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface SimpleInterestService {
	
	List<Response> getSimpleInterestList(Request request) throws JsonProcessingException;
	
}
