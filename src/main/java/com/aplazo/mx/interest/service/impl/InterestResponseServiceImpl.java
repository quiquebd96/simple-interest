package com.aplazo.mx.interest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplazo.mx.interest.entity.InterestResponse;
import com.aplazo.mx.interest.model.Response;
import com.aplazo.mx.interest.repository.SimpleInterestResponseRepository;
import com.aplazo.mx.interest.service.InterestResponseService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class InterestResponseServiceImpl implements InterestResponseService{
	
	@Autowired
	SimpleInterestResponseRepository simpleInterestResponseRepository;

	@Override
	public InterestResponse saveResponseInterest(List<Response> response, Long idRequest) throws JsonProcessingException {
		InterestResponse responseSave = new InterestResponse();
		ObjectMapper mapper = new ObjectMapper();
		String responseJSON = mapper.writeValueAsString(response);
		responseSave.setIdRequest(idRequest);
		responseSave.setResponse_json(responseJSON);
		return simpleInterestResponseRepository.save(responseSave);
	}

}
