package com.aplazo.mx.interest.service;

import java.util.List;

import com.aplazo.mx.interest.entity.InterestResponse;
import com.aplazo.mx.interest.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface InterestResponseService {
	
	InterestResponse saveResponseInterest(List<Response> response , Long idRequest) throws JsonProcessingException;

}
