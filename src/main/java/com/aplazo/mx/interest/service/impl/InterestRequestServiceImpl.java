package com.aplazo.mx.interest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplazo.mx.interest.entity.InterestRequest;
import com.aplazo.mx.interest.model.Request;
import com.aplazo.mx.interest.repository.SimpleInterestRequestRepository;
import com.aplazo.mx.interest.service.InterestRequestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class InterestRequestServiceImpl implements InterestRequestService {
	
	@Autowired
	SimpleInterestRequestRepository simpleInterestRequestRepository;
	
	@Override
	public InterestRequest saveRequestInteres(Request request) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJSON = mapper.writeValueAsString(request);
		InterestRequest requestSave = new InterestRequest();
		requestSave.setRequest_json(requestJSON);
		return simpleInterestRequestRepository.save(requestSave);
	}

}
