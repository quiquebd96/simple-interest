package com.aplazo.mx.interest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aplazo.mx.interest.model.Request;
import com.aplazo.mx.interest.model.Response;
import com.aplazo.mx.interest.service.SimpleInterestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("api/v1/simple-interest/hola")
public class SimpleInterestController {
	
	@Autowired
	SimpleInterestService simpleInterestService;
	
	 @Operation(summary = "Get the simple interest", description = "Simple Interest Payment List", tags={ "Request" })
	    @ApiResponses(value = { 
	        @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = Request.class))),	        
	        @ApiResponse(responseCode = "204", description = "No Content"),	        
	        @ApiResponse(responseCode = "500", description = "Internal Server Error") })
	@PostMapping()
	public ResponseEntity<?> postSimpleInterest(@RequestBody @Valid Request request){
		try {
			List<Response> payments = simpleInterestService.getSimpleInterestList(request);
			
			if (payments != null && !payments.isEmpty()) {
				return new ResponseEntity<List<Response>>(payments, HttpStatus.OK);
			}else {
				return new ResponseEntity<List<Response>>(payments, HttpStatus.NO_CONTENT);				
			}
			
		}catch(Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
